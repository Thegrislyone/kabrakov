document.querySelector(".header__breadcrumbs-catalog").onclick= function() {
    document.querySelector(".catalog").scrollIntoView({behavior:"smooth"});
}
document.querySelector(".header__breadcrumbs-delivery").onclick= function() {
    document.querySelector(".delivery").scrollIntoView({behavior:"smooth", block:"center"});
}
document.querySelector(".header__breadcrumbs-feedbacks").onclick= function() {
    document.querySelector(".feedback").scrollIntoView({behavior:"smooth", block:"center"});
}
document.querySelector(".header__breadcrumbs-payment").onclick= function() {
    document.querySelector(".payment").scrollIntoView({behavior:"smooth", block:"center"});
}
document.querySelector(".header__breadcrumbs-contacts").onclick= function() {
    document.querySelector(".contact").scrollIntoView({behavior:"smooth", block:"center"});
}

document.querySelector(".feedback__text").onscroll= function() {
    // console.log($(".feedback__text p").offset());
    if (Number($(".feedback__text p").offset().top) < 2600) {
        document.querySelector(".feedback__arrow").classList.add("rotate_arrow");    
    } else {
        document.querySelector(".feedback__arrow").classList.remove("rotate_arrow");
    }

}

